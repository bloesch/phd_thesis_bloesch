% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %2345678901234567890123456789012345678901234567890123456789012345678901234567890
% %        1         2         3         4         5         6         7         8
% 
% \documentclass[letterpaper, 10 pt, conference]{ieeeconf}  % Comment this line out if you need a4paper
% 
% 
% 
% \IEEEoverridecommandlockouts                              % This command is only needed if 
%                                                           % you want to use the \thanks command
% 
% \overrideIEEEmargins                                      % Needed to meet printer requirements.
% 
% % See the \addtolength command later in the file to balance the column lengths
% % on the last page of the document
% 
% % The following packages can be found on http:\\www.ctan.org
% \usepackage{graphics} % for pdf, bitmapped graphics files
% \usepackage{epsfig} % for postscript graphics files
% %\usepackage{mathptmx} % assumes new font selection scheme installed
% %\usepackage{times} % assumes new font selection scheme installed
% \usepackage{cleveref}
% \usepackage[cmex10]{amsmath}
% \usepackage{amssymb}
% \usepackage{units}
% 
% \usepackage{epstopdf}
% \epstopdfsetup{update}
% \epstopdfsetup{suffix=}
% 	
% \def\b#1{\boldsymbol{#1}}
% 
% \title{\LARGE \bf
% Fusion of Optical Flow and Inertial Measurements \\ for Robust Egomotion Estimation
% }
% 

% \author{Albert Author$^{1}$ and Bernard D. Researcher$^{2}$% <-this % stops a space
% \thanks{*This work was not supported by any organization}% <-this % stops a space
% \thanks{$^{1}$Albert Author is with Faculty of Electrical Engineering, Mathematics and Computer Science,
%         University of Twente, 7500 AE Enschede, The Netherlands
%         {\tt\small albert.author@papercept.net}}%
% \thanks{$^{2}$Bernard D. Researcheris with the Department of Electrical Engineering, Wright State University,
%         Dayton, OH 45435, USA
%         {\tt\small b.d.researcher@ieee.org}}%
% }
% \author{Michael Bloesch, Sammy Omari, P\'eter Fankhauser, Hannes Sommer, Christian Gehring, \\
%   Jemin Hwangbo, Mark A. Hoepflinger, Marco Hutter, Roland Siegwart \\
%   Autonomous Systems Lab, ETH Z\"{u}rich, Switzerland, bloeschm@ethz.ch
%   \thanks{This research was supported in part by the Swiss National Science Foundation (SNF)  through project $200021\_149427 / 1$ and the National Centre of Competence in Research Robotics.}}

% \begin{document}
% 
% \maketitle
% \thispagestyle{empty}
% \pagestyle{empty}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{abstract}
In this paper we present a method for fusing optical flow and inertial measurements. To this end, we derive a novel visual error term which is better suited than the standard continuous epipolar constraint for extracting the information contained in the optical flow measurements. By means of an unscented Kalman filter (UKF), this information is then \emph{tightly} coupled with inertial measurements in order to estimate the egomotion of the sensor setup. A nonlinear observability analysis is provided and supports the proposed method from a theoretical side. The filter is evaluated on real data together with ground truth from a motion capture system.
\end{abstract}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Introduction}
The use of cameras as light-weight egomotion sensors has been studied very broadly in the past few decades. The main advantage of a camera is that rich information can be obtained at relatively low power consumption. However, this information richness also poses the main difficulty, as the vast amount of information needs to be handled properly before the egomotion can be inferred.

Within the computer vision community, Davison \cite{Davison2003} presented one of the first algorithms that is able to accurately track the 3D pose of a monocular camera. His idea was to design an Extended Kalman Filter (EKF) which simultaneously tracks the pose of the camera as well as the 3D position of points of interest, whereby the reprojection errors of the perceived features serve as innovation term. In the following, different authors presented adaptations in order to tackle different weaknesses of this approach, such as feature initialization \cite{Montiel2006} and limited map size \cite{clemente2007}.

Compared to the above mentioned \emph{non-delayed} approaches, \emph{delayed} methods also take past robot poses and measurements into account. The delayed approaches have become popular with the work of Klein and Murray \cite{Klein2007}: Based on a subset of camera frames (keyframes) a bundle adjustment algorithm \cite{Triggs2000} optimizes a map, while the actual pose of the camera is tracked by minimizing the reprojection error between map and camera. Strasdat et al. \cite{Strasdat2010} argued that in terms of accuracy and computational costs it would be more beneficial to increase the number of tracked features rather than the number of frames they are tracked in. In the following, the limits of vision-only state estimation and mapping where pushed even further by various other elaborate delayed frameworks \cite{Mei2011,Strasdat2011,Kaess2012}.

In parallel to the ``vision-only'' based approaches, other researchers started including inertial measurements into their estimation algorithms. Relying on a known visual pattern, Mirzaei and Roumeliotis \cite{Mirzaei2008} showed one of the first online methods for extrinsic Inertial Measurement Unit (IMU)-camera calibration and IMU bias estimation. Later, Kelly and Sukhatme \cite{Kelly2011}, Jones and Soatto \cite{Jones2011}, as well as Weiss et al. \cite{Weiss2013} presented different frameworks for visual-inertial navigation including the co-estimation of calibration parameters. All of these authors emphasize the importance of analyzing the observability characteristics of the underlying system and discuss the related issues. Recently, Leutenegger et al. \cite{Leutenegger2013} presented a delayed framework in which the authors included visual and inertial error terms into a extended nonlinear optimization in order to estimate the motion of a stereo camera as well as the landmarks in the map.

Efforts have also been done in order to find other visual error terms for combining the image information with inertial measurements. For example, Diel et al. \cite{Diel2005} directly use the epipolar constraint between two matching features in subsequent frames as innovation term for their Kalman filter and thereby fuse the visual information with the accelerometer measurements (the gyroscopes and attitude are handled separately). By making the assumption that all features lie on a single plane, Omari et al. \cite{Omari2013} derive a visual error term for optical flow measurements and combine it with inertial measurements by means of an UKF. Both approaches have in common that the 3D position of the features are not included into the state of the filter which significantly reduces the computational costs. Similarly, Mourikis and Roumeliotis \cite{Mourikis2007} also exclude the position of the features from the states of their filter and introduce a measurement model in order to account for the information when a feature is measured in multiple camera frames.

The primary goal of the present work is to propose a simple and reliable framework for the estimation of quantities which are critical for the safe operation of autonomous robots. We want to emphasize that we do not focus on achieving high-precision position and attitude accuracy, rather, our goal is to achieve a robust estimation of the velocity and inclination angle of the robot. This is especially important for systems which are controlled through dynamic motion, such as legged robots or quadrocopters. For this reason, we introduce visual error term which can directly extract information from a single feature match and does not rely on repeated measurements of the same feature. The above mentioned work of Diel et al. \cite{Diel2005} is the closest to the present approach. In contrast to it, we propose the use of a different visual error term and co-estimate the inverse scene depth. By means of an UKF, we carry out a \emph{tight} fusion of the visual and inertial measurements, whereby gyroscope and accelerometer measurement are included during the prediction step and the visual error terms serve as innovation during the update step. The presented approach is supported by a full nonlinear observability analysis and evaluated on data from real experiments.

The remainder of this paper is structured as follows: After introducing the most important notations and conventions in \cref{flow_sec:pre}, we describe the structure of the filter including the prediction and update steps in \cref{flow_sec:filter}. In \cref{flow_sec:obsv} we show and discuss the result of the nonlinear observability analysis. The experimental setup is described in \cref{flow_sec:expset}. Finally, we discuss the obtained results in \cref{flow_sec:dis} and conclude with \cref{flow_sec:con}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Prerequisites}\label{flow_sec:pre}
% TODO: rewrite and adapt
For better readability we give a short overview on the employed notations and conventions. The coordinates, expressed in a frame $A$, of a vector from a point $P$ to a point $Q$ are denoted by ${}_A\b{r}_{PQ}$. If $B$ is a second coordinate frame, then $\b{C}_{BA}$ maps the coordinates expressed in $A$ to the corresponding coordinates in $B$. The rotation between both frames is generally parametrized by the unit quaternion $\b{q}_{BA}$, with the corresponding mapping $\b{C}: \b{q}_{BA} \mapsto \b{C}_{BA}$. Throughout the paper, we add a subscript $k$ to a quantity $v$, if we want to talk about its value at a time $t_k$, i.e., $v_k = v(t_k)$. Two coordinate frames are of interest: the world fixed coordinate frame $W$ and the sensor frame $B$. For the sake of simplicity the following derivation assumes that the camera and the IMU coordinate frames are aligned with $B$.

We handle rotations as elements of $SO(3)$, where, together with the exponential and logarithm map, difference and derivatives are defined on $\mathbb{R}^{3}$. This is of high importance for the setup of the filter as well as for the corresponding observability analysis. Please note, that for this reason, also derivatives containing quaternions will be three dimensional in the corresponding directions, e.g. $\dot{\b{q}} = - \b{\omega} \in \mathbb{R}^{3}$. More information on this can be found in our previous work \cite{Bloesch2013b}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Filter Setup}\label{flow_sec:filter}
\subsection{Optical Flow and Visual Error Term}\label{flow_sec:OF}
Based on the assumption of a static scene the following identity can be directly derived using kinematics relations only:
\begin{align}
	0 =& {}_B\b{v}_B + ({}_B\b{w}_B^\times \b{m}_i + \b{u}_i) \lambda_i + \b{m}_i \dot \lambda_i,
\end{align}
where ${}_B\b{v}_B$ and ${}_B\b{w}_B$ are the robot-centric velocity and rotational rate. The quantities $\b{m}_i$, $\b{u}_i$ and $\lambda_i$ are related to the optical flow of a static feature $i$ and represent the unit length bearing vector, the optical flow vector, and the depth of the feature. The challenge here is to find a way to properly extract information out of the equation without having to co-estimate the depth (and it's derivative) for each single optical flow measurement. A very common approach is to employ the continuous epipolar constraint which results from the above equation if left-multiplied by $\b{m}_i^T ({}_B\b{w}_B^\times \b{m}_i + \b{u}_i)^\times$:
\begin{align}
	0 =& \b{m}_i^T ({}_B\b{w}_B^\times \b{m}_i + \b{u}_i)^\times {}_B\b{v}_B.
\end{align}
This corresponds to an analytical elimination of the depth and its derivative. The problem is that this reduction does not consider the stochastic nature of the system and draws the estimation process towards singularities, e.g. zero velocity, which don't correspond to the maximum likelihood estimate (which is in general a desirable target for estimation). As a trade-off we propose to eliminate the derivative of the depth analytically by left-multiplying the equation by a $2\times3$ matrix $\b{M}_i$ which fulfills:
\begin{align}
	\b{M}_i \b{m}_i = 0 \ \ \wedge \ \ \b{M}_i \b{M}_i^T = \b{I}_2.
\end{align}
Additionally we make use of an inverse-depth parametrization, $\alpha_i = 1/\lambda_i$, and obtain
\begin{align}
	0 =& \b{M}_i \left( {}_B\b{v}_B \alpha_i + ({}_B\b{w}_B^\times \b{m}_i + \b{u}_i) \right).
\end{align}
In comparison to the continuous epipolar constraint, this term retains more of the original constraint and is less susceptible to singularities. However, it also still contains one additional unknown, $\alpha_i$, per visual feature. In order to cope with this, we will assume that the inverse depths $\alpha_i$ exhibit a Gaussian distribution around a mean $\alpha$ with standard deviation $\sigma_\alpha$. The new parameter $\alpha$ corresponds to the inverse scene depth and will be co-estimated in the estimation process.

\subsection{Filter States and Prediction Equations}
The states of a filter have to be selected such that appropriate prediction and measurement equation can be derived. We define the following filter states:
\begin{align}
	\b{x} :=& \begin{pmatrix} \b{r}, \b{v}, \b{q}, \b{c}, \b{d}, \alpha \end{pmatrix}, \\
	:=& \begin{pmatrix} {}_W\b{r}_{WB}, {}_B\b{v}_{B}, \b{q}_{WB}, {}_B\b{b}_f, {}_B\b{b}_\omega, \alpha \end{pmatrix},
\end{align}
where $\b{r}$ is the world position of the sensor, $\b{v}$ represents its robot-centric velocity, $\b{q}$ parametrizes the rotation between the sensor and the world coordinate frame, and $\b{c}$ and $\b{d}$ are the biases of the accelerometer and gyroscope. The additional state $\alpha$ is the inverse scene depth which is used for incorporating the optical flow measurements. The advantage of the robot-centric choice of states is that we thereby partition the state into non-observable states (absolute position and yaw) and observable states and thus avoid numerical problems related to non-observable states. A small drawback is that the noise of the gyroscope propagates onto the velocity state as well. Since, as will be shown later, the robot-centric velocity is fully observable, the additional noise can be compensated by the filter.

Analogous to other fusion algorithms including inertial measurements, we embed the proper acceleration measurement $\tilde{\b{f}}$ and the rotational rate measurement $\tilde{\b{\omega}}$ of the IMU directly into the prediction step of the proposed filter. Assuming that both measurements are affected by white Gaussian noise, $\b{w}_{f}$ and $\b{w}_{\omega}$, and additive bias terms, $\b{c}$ and $\b{d}$, we can write down
\begin{align}
	\tilde{\b{f}} =& \ \b{f} + \b{c} + \b{w}_f, \\
	\tilde{\b{\omega}} =& \ \b{\omega} + \b{d} + \b{w}_\omega.
\end{align}
Both quantities are related to the kinematics of the sensor by
\begin{align}
	\b{f} =& \ \b{C}(\b{q}_{BW})\left({}_W\dot{\b{v}}_B - \b{g}\right), \label{flow_eq:proacc} \\
	\b{\omega} =& - \dot{\b{q}}_{BW}, \label{flow_eq:rotrat}
\end{align}
where $\b{g}$ is the gravity vector in $W$. By evaluating the total derivative of the filter states and combining it with the inertial measurements we obtain the following continuous time differential equations:
\begin{align}
	\dot{\b{r}} =& \ \b{C}(\b{q}) \b{v} + \b{w}_r, \label{flow_eq:sde1}\\
	\dot{\b{v}} =& -(\tilde{\b{\omega}} - \b{d} - \b{w}_\omega)^\times \b{v} + \tilde{\b{f}} - \b{c} - \b{w}_f + \b{C}^T(\b{q}) \b{g}, \\
	\dot{\b{q}} =& \ \b{C}(\b{q}) (\tilde{\b{\omega}} - \b{d} - \b{w}_\omega), \\
	\dot{\b{c}} =& \ \b{w}_{c}, \\
	\dot{\b{d}} =& \ \b{w}_{d}, \\
	\dot{\alpha} =& \ w_{\alpha}. \label{flow_eq:sde6} 
\end{align}
The additional continuous white Gaussian noise processes $\b{w}_{c}$ and $\b{w}_{d}$ model a certain drift affecting the bias terms. $w_{\alpha}$ is included in order to handle varying inverse scene depths and $\b{w}_r$ is included for being able to excite the full filter state and for modeling errors caused by the subsequent discretization of the states. For all white Gaussian noise processes, the corresponding covariance parameters, $\b{R}_{r}$, $\b{R}_{f}$, $\b{R}_{\omega}$, $\b{R}_{c}$, $\b{R}_{d}$, and $\b{R}_{\alpha}$ describe the magnitude of the noise. Except for $\b{R}_{r}$ and $\b{R}_{\alpha}$ which are tuning parameters, all covariance parameters can be identified by considering the Allan plots of the IMU measurements \cite{Sheimy2008}.

The discretization is based on a simple Euler forward integration scheme. Please note that for the rotational states, the step forward can be taken on the corresponding sigma algebra and then be mapped back onto $SO(3)$. This corresponds to:
\begin{align}
	\b{q}(t_k) =& \exp\left(\Delta t_{k} \dot{\b{q}}(t_{k-1})\right) \otimes \b{q}(t_{k-1}),
\end{align}
with
\begin{align}
	\Delta t_{k} = \ t_{k}-t_{k-1}.
\end{align}
This leads to:
\begin{align}
	\b{r}_{k} =& \ \b{r}_{k-1} + \Delta t_{k} \left(\b{C}_{k-1} \b{v}_{k-1} + \b{w}_{r,k}\right), \label{flow_eq:pre1} \\
	\b{v}_{k} =& \left( I - \Delta t_{k} \left(\tilde{\b{\omega}}_{k} - \b{d}_{k-1} - \b{w}_{\omega,k}\right)^\times \right) \b{v}_{k-1} \nonumber \\
	& + \Delta t_{k} \Big(\tilde{\b{f}}_{k} - \b{c}_{k-1} - \b{w}_{f,k} + \b{C}^T_{k-1} \b{g} \Big), \\
	\b{q}_{k} =& \exp\Big(\Delta t_{k} \b{C}_{k-1} (\tilde{\b{\omega}}_{k} - \b{d}_{k-1} - \b{w}_{\omega,k})\Big) \otimes \b{q}_{k-1}, \\
	\b{c}_{k} =& \ \b{c}_{k-1} + \Delta t_{k} \b{w}_{c,k}, \\
	\b{d}_{k} =& \ \b{d}_{k-1} + \Delta t_{k} \b{w}_{d,k}, \\
	\alpha_{k} =& \ \alpha_{k-1} + \Delta t_{k} w_{\alpha,k}. \label{flow_eq:pre6}
\end{align}

\subsection{Measurement Equations}
The measurement equations are directly based on the findings of \cref{flow_sec:OF}. For each available optical flow measurement $i$ we directly define the corresponding 2D innovation term for the filter:
\begin{align}
	\b{y}_i =& \b{M}_i \left( \b{v} \ \alpha_i + (\b{\omega}^\times \b{m}_i + \b{u}_i) \right).
\end{align}
As discussed above, we introduced the inverse scene depth as a filter state and thus model deviations of the single inverse depths $\alpha_i$ as measurement noise:
\begin{align}
	\alpha_i = \alpha + n_{\alpha,i}, \ \ n_{\alpha,i} \sim \mathcal{N}(0,\sigma^2_\alpha).
\end{align}
Furthermore, we also have to model noise on the bearing vectors $\b{m}_i$ and optical flow vectors $\b{u}_i$. For typical scenarios the major part of the uncertainties originate through $\b{u}_i$, which lies in the orthogonal subspace of $\b{m}_i$. Thus, we can introduce an additive lumped noise term on $\b{u}_i$, whereby it is sufficient to excite directions orthogonal to $\b{m}_i$ only. This can be achieved by means of the previously defined matrix $\b{M}_i$ ($\b{n}_u$ is two dimensional):
\begin{align}
	\tilde{\b{u}}_i &= \b{u}_i - \b{M}_i^T \b{n}_u, \\
	\b{n}_u &\sim(0,\b{R}_u).
\end{align}
With this the innovation term becomes:
\begin{align}
	\b{y}_i =& \b{M}_i \left( \b{v} ( \alpha + n_{\alpha,i}) + (\b{\omega}^\times \b{m}_i + \tilde{\b{u}}_i) \right) + \b{n}_u.
\end{align}
The parameter $\b{R}_u$ describes the accuracy of the visual measurements and the parameter $\sigma^2_\alpha$ depends on the variance of the inverse depths in the scene.

An interesting effect is that whenever the velocity is small or when the inverse scene depth tends towards zero (i.e. the scene is far away), the innovation term will be equivalent to a visual gyroscope:
\begin{align}
	\b{y}_i^* =& \b{M}_i \left((\b{\omega}^\times \b{m}_i + \tilde{\b{u}}_i) \right) + \b{n}_u. \label{flow_eq:up}
\end{align}

\subsection{Unscented Kalman Filter and Outliers Detection}
An unscented Kalman filter (UKF) is employed as filtering framework. The main reason for this is that the UKF can handle correlated noise between prediction and update by using a single set of augmented sigma points for both steps. All equations required for its implementation are the prediction equation (\ref{flow_eq:pre1})-(\ref{flow_eq:pre6}) and the update equation (\ref{flow_eq:up}), whereby the single innovation terms of the multiple features are stuck together. The twofold use of the gyroscope measurement can be directly seen in these equations. Please note that the implementation has to take into account that, although the attitude is parametrized by a unit quaternion, the corresponding noise and perturbations are always on a 3D subspace. For a detailed discussion on the employed UKF itself please refer to \cite{Julier2002}.

In order to handle the high sensitivity of Kalman filters to outliers, we implement a simple outliers detection method on the innovation terms. Using an analogous approach as Mirzaei et al. \cite{Mirzaei2008}, we reject a visual measurement whenever the Mahalanobis distance of the corresponding innovation terms exceeds a certain threshold. The predicted covariance of the innovation is used as weighting for the Mahalanobis distance and the threshold is chosen in such a manner that, in theory, 5\% of the inliers are rejected. Considering that the underlying probability distribution is a $\chi^2$-distribution with two degrees of freedom the threshold is set to $p = 5.99$. In summary, the criteria for rejecting a measurement $i$ is given by (where $\b{S}_{i}$ is the predicted covariance matrix):
\begin{align}
	\b{y}_{i}^T \b{S}_{i}^{-1} \b{y}_{i} > p.
\end{align}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Observability Analysis}\label{flow_sec:obsv}
A nonlinear observability analysis is carried out for the proposed system. A detailed discussion of the theory behind it was provided by Hermann and Krener \cite{Hermann1977}. In the scope of this paper we only outline the rough procedure of the analysis. Based on the nonlinear representation of the system an observability matrix is derived in order to assess the observability characteristics of the system. The system can be written as follows, whereby the noise quantities can be ignored since they don't affect the observability analysis:
\begin{align}
  \dot{\b{x}} =& \begin{pmatrix}
    \b{C} \b{v} \\
    \hat{\b{\omega}}^\times \b{v} - \hat{\b{f}} + \b{C}^T \b{g} \\
    - \b{C} \hat{\b{\omega}} \\
    0 \\
    0 \\
    0
  \end{pmatrix}, \\
  \b{h}_i(\b{x}) =& \b{M}_i \left( \b{v} \ \alpha + ( - \hat{\b{w}}^\times \b{m}_i + \tilde{\b{u}}_i) \right),
\end{align} 
with the shortcuts
\begin{align}
  \hat{\b{f}} =& -\tilde{\b{f}} + \b{c}, \\
  \hat{\b{\omega}} =& -\tilde{\b{\omega}} + \b{d}.
\end{align}
The observability matrix is composed of the gradient of the Lie derivatives of the above system, whereby $\tilde{\b{f}}$ and $\tilde{\b{\omega}}$ are, in the context of this analysis, the inputs to the system. We can show, that if there are three optical measurements with non-coplanar bearing vectors and if the inverse scene depth is not zero we can simplify the observability matrix to the following term (if $\alpha=0$ only the gyroscope bias and the inverse scene depth itself (if $\b{v} \neq 0$) are observable):
\begin{align}
  \b{O} = \begin{bmatrix}
    0 & \b{I} & 0 & 0 & 0 & \frac{1}{\alpha} \b{v} \\
    0 & 0 & 0 & 0 & \b{I} & 0 \\
    0 & 0 & \b{C}^T \b{g}^\times & - \b{I} & 0 & \b{C}^T \b{g} - \hat{\b{f}} \\
    0 & 0 & \hat{\b{\omega}}^\times \b{C}^T \b{g}^\times & 0 & 0 & \hat{\b{\omega}}^\times \b{C}^T \b{g}
  \end{bmatrix}.
\end{align}
Throughout the analysis only rank-preserving row operations are carried out which keeps the relation between each column and a specific state of the filter. We also have to keep in mind, that $\tilde{\b{f}}$ and $\tilde{\b{\omega}}$ represent system inputs in this analysis, and thus a single line in the matrix can be duplicated by inserting different values for $\tilde{\b{f}}$ and $\tilde{\b{\omega}}$ (see \cite{Hermann1977}). By inserting two non-colinear values for $\tilde{\b{\omega}}$ (through  $\hat{\b{\omega}}$) in the last row of the matrix we can further simplify the matrix to:
\begin{align}
  \b{O} = \begin{bmatrix}
    0 & \b{I} & 0 & 0 & 0 & \frac{1}{\alpha} \b{v} \\
    0 & 0 & 0 & 0 & \b{I} & 0 \\
    0 & 0 & 0 & - \b{I} & 0 & - \hat{\b{f}} \\
    0 & 0 & \b{C}^T \b{g}^\times & 0 & 0 & \b{C}^T \b{g}
  \end{bmatrix}.
\end{align}
The rank of this matrix is $12$ (independent of the choice of $\b{C}$, $\b{v}$, or $\hat{\b{f}}$) and the dimension of the right null-space is consequently $4$, which is spanned by the following matrix:
\begin{align}
  \b{N} = \begin{bmatrix}
    \b{I} & 0 \\
    0 & 0 \\
    0 & \b{g} \\
    0 & 0 \\
    0 & 0 \\
    0 & 0
  \end{bmatrix}.
\end{align}
In an informal way, the perturbations along the directions spanned by $\b{N}$ cannot be perceived at the filter output. While the first column corresponds to the absolute position of the system, the second column represents a rotation around the gravity axis, i.e., global position and yaw angle are not observable. Mathematically this can be written as:
\begin{align}
  \b{r}^* =& \b{r} + \delta \b{r}, \\
  \b{q}^* =& \exp\left(\b{g} \delta \psi \right) \otimes \b{q},
\end{align}
where $\delta \b{r}$ and $\delta \psi$ are perturbations. $\b{r}$ and $\b{q}$ cannot be distinguished from $\b{r}^*$ and $\b{q}^*$, respectively.

All in all, the above nonlinear observability analysis allows us to state that for all points in the state-space (except if $\alpha = 0$) there \emph{exists} some input $\tilde{\b{f}}$ and $\tilde{\b{\omega}}$ (corresponding to a certain motion of the sensor) such that all states are locally weakly observable, except for the global position and yaw angle.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Experimental Setup}\label{flow_sec:expset}
\begin{figure}[t]
\centering
\includegraphics[width=0.8\columnwidth]{img/DSC09713-edited2.png}
% \vspace{-8mm}
\caption{ASL visual-inertial SLAM sensor employed for evaluating the presented optical flow and inertial measurement fusion approach.}\label{flow_fig:sensor}
% \vspace{-5mm}
\end{figure}
To validate the proposed scheme, the Unscented Kalman filter was implemented in C++. The filter was tested on data that were recorded using the ASL visual-inertial SLAM sensor (see \cref{flow_fig:sensor}), with synchronized global-shutter camera (Aptina MT9\-V034 at 20 Hz) and IMU (Analog Devices ADIS16488 at 200 Hz). The pose of the sensor was additionally tracked using a Vicon motion tracking system at 100 Hz.

The image features are tracked using a Lukas-Kanade-based tracker. Salient image features that are used for tracking are extracted by first applying a FAST corner detector, computing the Shi-Tomasi score for each extracted corner and then selecting those corners which have the highest score while ensuring a uniform distribution of the features in the image. A uniform feature distribution is ensured by masking parts of the images that are already populated with strong features and by only adding new, weaker features in unpopulated image regions.

Feature extraction and LK-tracking for 150 features is taking less than 2.5 ms in total on a single core of an Intel i7-3740QM processor for one frame. Equivalently, a measurement update step using 50 optical flow features is performed in 10 ms. During the experiments an average feature count of 50 features was used. The rather bad scalability of the filter update can be easily overcome by changing to the information form of the filter, which will be part of future work.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Results and Discussion}\label{flow_sec:dis}
The presented approach was evaluated on different datasets from an indoor environment where the feature depths range between \unit[0.5]{m} and \unit[5]{m}. The motion of the sensor included rotational rate of up to \unit[3]{rad/s}. Our main goal was to develop a filter for delivering high-rate and reliable state estimates rather than being mainly focused on estimation accuracy. Furthermore, the main states of interest are the velocities and the inclination angles since they are of major importance if it comes to control of dynamic robot motions. Using a $2$ minute long dataset where the sensor was excited along its different degrees of freedom, the following RMS values where obtained:
\begin{itemize}
 \item Attitude (rad): 0.027 (roll), 0.005 (pitch) , 0.074 (yaw)
 \item Velocity (m/s): 0.058 (x),  0.070 (y),  0.075 (z)
\end{itemize}
whereby the velocity is always evaluated in the sensor frame $B$. When using different datasets with similar motions the RMS values fluctuate around the above values, except for the RMS of the yaw angle which increases with the estimation time (since it is not observable). The estimated IMU biases converge relatively fast depending on the motion of the system. While we have no ground truth values for the bias terms, figure \ref{flow_fig:acc_bias} and \ref{flow_fig:gyr_bias} show the typical convergence of the biases when the system is being excited along its different directions. Figuring out which direction needs to be excited for improving the estimation of a certain state can be a very difficult problem and is not within the scope of this paper. The $3\sigma$-bounds of the covariance matrix are plotted as dashed lines.
\begin{figure}[t]
\centering
\includegraphics[width=0.8\columnwidth]{img/acc_bias.eps}
% \vspace{-8mm}
\caption{Estimated accelerometer biases. Red: estimated values. Red dashed line: $3\sigma$-bound. The initial converges is supported by motion of the sensor. The estimate is more accurate along the x-axis because it is more often aligned with the gravity axis.}\label{flow_fig:acc_bias}
% \vspace{-5mm}
\end{figure}
\begin{figure}[t]
\centering
\includegraphics[width=0.8\columnwidth]{img/gyr_bias.eps}
% \vspace{-8mm}
\caption{Estimated gyroscope biases. Red: estimated values. Red dashed line: $3\sigma$-bound. The states here converge faster than the accelerometer biases since the optical flow measurement have a direct impact on the angular rates.}\label{flow_fig:gyr_bias}
% \vspace{-5mm}
\end{figure}


Figure \ref{flow_fig:pos_stop}, \ref{flow_fig:vel_stop}, and \ref{flow_fig:rpy_stop} present the results from a dataset where after some initial motion the sensor holds still for awhile before being moved again. This can be clearly seen between $33-43$ seconds. In contrast to the standard epipolar constraint, the employed visual error term still extracts information from the optical flow measurements analogous to a visual gyroscope. Still, during this phase additional uncertainty accumulates in the different states. However, as soon as the sensor is moved again, the observable states very quickly converge back to the reference. This can be nicely observed for the velocity estimates. Note as well, that although the position of the sensor is unobservable, it can be corrected and loose uncertainty to some extent through the cross-correlation it maintains with the other states.
\begin{figure}[t]
\centering
\includegraphics[width=0.8\columnwidth]{img/pos_stop.eps}
% \vspace{-8mm}
\caption{Estimated sensor position. Red: estimated values. Red dashed line: $3\sigma$-bound. Dashed blue line: motion capture ground truth. The position state is affected by increasing uncertainty since it is not observable and represents the integration of the velocity estimate.}\label{flow_fig:pos_stop}
% \vspace{-5mm}
\end{figure}
\begin{figure}[t]
\centering
\includegraphics[width=0.8\columnwidth]{img/vel_stop.eps}
% \vspace{-8mm}
\caption{Estimated sensor velocity expressed in the sensor coordinate frame itself. Red: estimated values. Red dashed line: $3\sigma$-bound. Dashed blue line: motion capture ground truth. The robot-centric velocity is fully observable and consequently has a bounded uncertainty. Even after a phase of increased uncertainty it is able to recover if sufficient excitation is available.}\label{flow_fig:vel_stop}
% \vspace{-5mm}
\end{figure}
\begin{figure}[t]
\centering
\includegraphics[width=0.8\columnwidth]{img/rpy_stop.eps}
% \vspace{-8mm}
\caption{Roll, pitch, and yaw angle of the sensor. Red: estimated values. Red dashed line: $3\sigma$-bound. Dashed blue line: motion capture ground truth. Pitch and roll are observable and consequently exhibit a nice tracking behavior. Yaw is not observable and slowly drifts away.}\label{flow_fig:rpy_stop}
% \vspace{-5mm}
\end{figure}

When replacing the presented visual error term by other terms such as the simple continuous epipolar constraint or normalized forms of it, the observed results became worse. Very often the estimation process would be drawn to zero (e.g. for the continuous epipolar constraint) or very quickly lead to bad tracking or divergence.

All in all the filter exhibits a rather average performance in terms of accuracy when compared with the state of the art visual-inertial algorithms. However, when considering that only frame to frame (20 Hz) information is included into the filter, the obtained results are relatively surprising, especially since other quantities like the IMU biases have to be co-estimated simultaneously. A major advantage of this approach is that the filter is free of any complex initialization procedure and only relies on single feature matches between subsequent frames. It does not require the long term tracking of some feature and is thus much less affected by fast motions.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Conclusion and Future Work}\label{flow_sec:con}

In this paper we presented a relative simple approach for fusing optical flow and inertial measurements. By deriving a special optical flow error term and embedding it into an UKF framework, we were able to derive a filter for estimating the egomotion of the sensor, the IMU biases as well as the inverse scene depth. By carrying out a nonlinear observability analysis we showed that all states except for the global position and yaw angle are locally weakly observable. The results obtained on a real dataset confirmed that the filter was able to estimate the different observable states.

One important aspect of future work will be the combination of the presented approach with other visual localization methods. While the strength of the presented approach lies in its robustness and speed, it could be combined together with some static feature tracking in order to improve its accuracy and long term stability. Other possible extensions include the implementation on multiple cameras or the combination with further sensor modalities.

% 
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% 
% \bibliographystyle{IEEEtranS}
% \bibliography{ref.bib}
% 
% 
% 
% \end{document}
