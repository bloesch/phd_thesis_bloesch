\ProvidesClass{thesis_asl}[2016/11/10]
\LoadClassWithOptions{scrbook}

\newcommand{\linkcolor}{gray}
\DeclareOption{print}{\renewcommand{\linkcolor}{black}}
\ProcessOptions\relax

\RequirePackage{color}
\RequirePackage{bibentry}
\RequirePackage{amssymb}
\RequirePackage{amsmath}
\RequirePackage{amsfonts}
\RequirePackage[T1]{fontenc}
\RequirePackage{verbatim}
\RequirePackage[utf8]{inputenc}
\RequirePackage{url}
\RequirePackage{units}
\RequirePackage{graphicx}
\RequirePackage{epstopdf}
\RequirePackage[table]{xcolor}
\RequirePackage[colorlinks,bookmarksopen,bookmarksnumbered,citecolor=\linkcolor,urlcolor=\linkcolor,menucolor=\linkcolor,allcolors=\linkcolor]{hyperref}
\RequirePackage{cleveref} % Must come last
\RequirePackage[sort&compress,sectionbib,numbers]{natbib}
\RequirePackage{acronym}
\RequirePackage{anyfontsize}

%%%%%%%%% Random Options %%%%%%%%%
\setcapindent{0px} % No indentation for captions
\setcapwidth{0.9\columnwidth}
\setkomafont{captionlabel}{\normalfont\normalcolor\bfseries}
\nobibliography* % Allows the use of bibentry
\epstopdfsetup{update}
\epstopdfsetup{suffix=}
\renewcommand{\floatpagefraction}{.65}

%%%%%%%%% Formating of chapter heading %%%%%%%%%
\def\@makechapterhead#1{%
  { \setlength{\parskip}{0px} % fixes distances within header
    \parindent 0px
    \vspace*{20px}
    \raggedleft
    \huge\bfseries\sffamily \textcolor[rgb]{0.7,0.7,0.7}{\@chapapp{} {\fontsize{80}{100}\selectfont \thechapter}} \\[30px]
    \raggedright
    \huge\bfseries\sffamily #1 \\[-10px]
    \rule{\linewidth}{0.2mm} \\
    \vspace{20px}
  }
}

\def\@makeschapterhead#1{
  { \setlength{\parskip}{0px} % fixes distances within header
    \parindent 0px
    \vspace*{20px}
    \raggedright
    \huge\bfseries\sffamily #1 \\[-10px]
    \rule{\linewidth}{0.2mm} \par
    \vspace{20px}
  }
}

%%%%%%%%% Formating of head and foot %%%%%%%%%
\setkomafont{pageheadfoot}{%
  \normalfont\normalcolor\sffamily\slshape
}
\setkomafont{pagenumber}{%
  \normalfont\normalcolor\sffamily
}

%%%%%%%%% Title Pages %%%%%%%%%
\newenvironment{titlepages}{
  % Define parameter tokens
  \newtoks\thesistitle
  \newtoks\thesisauthor
  \newtoks\thesisyear
  \newtoks\thesisnum
  \newtoks\thesisdepartment
  \newtoks\thesisacatitle
  \newtoks\thesisdateofbirth
  \newtoks\thesiscitizen
  \newtoks\thesisexaminer
  \newtoks\thesiscoexaminerA
  \newtoks\thesiscoexaminerB
  \newtoks\thesiscoexaminerC
  \newtoks\thesisdedication
}{ 
  % Title page
  \thispagestyle{empty}
  \begin{center}
    \vspace*{0mm}
    {\normalsize\noindent DISS.\ ETH NO.\ \the\thesisnum} 
    \vfill
    {\LARGE\textsc{\textbf{\the\thesistitle}}} \\
    \vspace{3mm plus 0.6fill}
    \normalsize  A thesis submitted to attain the degree of \\
    \vspace{3mm}
    DOCTOR OF SCIENCES of ETH ZURICH \\
    \vspace{3mm}
    (Dr. sc. ETH Zurich) \\
    \vspace{0mm plus 0.6fill}
    presented by \\
    \vspace{3mm}
    {\Large{\textsc{\the\thesisauthor}}} \\
    \vspace{3mm}
    \the\thesisacatitle \\
    \vspace{3mm}
    born on \the\thesisdateofbirth \\
    citizen of \the\thesiscitizen
    \vfill
    accepted on the recommendation of \\
    \vspace{3mm}
    \the\thesisexaminer, Examiner \\
    \the\thesiscoexaminerA, Co-examiner \\
    \if\relax\the\thesiscoexaminerB\relax\else
      \the\thesiscoexaminerB, Co-examiner \\
    \fi
    \if\relax\the\thesiscoexaminerC\relax\else
      \the\thesiscoexaminerC, Co-examiner \\
    \fi
    \vspace{0mm plus 0.6fill}
    \the\thesisyear
  \end{center}
  \newpage

  % Second page
  \thispagestyle{empty}
  \vspace*{0pt plus 0.4fill}
  \begin{center}\normalfont
    \itshape \the\thesisdedication
  \end{center}
  \vspace{0pt plus 0.6fill}
  \begin{flushleft}\normalfont
    \the\thesisdepartment\\
    ETH Zurich\\ 
    Switzerland\\
    \vspace{20mm}
    \copyright{} \the\thesisyear{} \the\thesisauthor. All rights reserved.
  \end{flushleft}
  \newpage
}


%%%%%%%%% Paper Environment %%%%%%%%%
\newcounter{paper}
\newif\ifcompilepapers
\compilepaperstrue

\newenvironment{paper}[3]{
  % Define parameter tokens
  \newtoks\papertitle
  \newtoks\paperauthor
  \newtoks\paperabstract
  \newtoks\paperfolder
  \newtoks\paperfile
  \newtoks\paperlabel
  \newtoks\paperchaptermark
  \newtoks\paperpublishedin
  \newtoks\paperyear
  \newtoks\papereditor
  \newtoks\paperaddendum
  % Pass options (cannot directly be accessed in closing of environment
  \paperfolder = {#1}
  \paperfile = {#2}
  \paperlabel = {#3}
}{
  % Adapt TOC depth and counters
  \addtocontents{toc}{\protect\setcounter{tocdepth}{1}}
  \stepcounter{paper}
  \stepcounter{chapter}
  
  % Overwrite commands
  \renewcommand*\thesection{\arabic{section}}
  \renewcommand{\chaptermark}[1]{\markboth{##1}{}}
  \renewcommand{\maketitle}{\relax}
  \newenvironment{abstract}{\comment}{\endcomment}
  \renewcommand{\@makeschapterhead}[1]{}
  
  % Title page
  \cleardoublepage
  % Create label and hyperlink targets
  \makeatletter
  \protected@write \@auxout {}{\string \newlabel {\the\paperlabel}{{
      \the\paperauthor,
      ``\the\papertitle''.
      \if\relax\the\papereditor\relax
	In
      \fi
      {\slshape \the\paperpublishedin},
      \if\relax\the\papereditor\relax \else
	{\the\papereditor}
      \fi
      \the\paperyear.
  }{\thepage}{}{\the\paperlabel}{}}}
  \protected@write \@auxout {}{\string \newlabel {\the\paperlabel_short}{{Paper \Roman{paper}}{\thepage}{}{\the\paperlabel}{}}}
  \hypertarget{\the\paperlabel}{\relax}
  \makeatother
  \chapter*{Paper \Roman{paper}: \the\papertitle}
  % Add entry to toc
  \addcontentsline{toc}{chapter}{Paper \Roman{paper}: \the\papertitle}
  % Adapt chapter mark if necessary
  \chaptermark{Paper \Roman{paper}: 
    \if\relax\the\paperchaptermark\relax
      \the\papertitle
    \else
      \the\paperchaptermark
    \fi
  }
  % Actual title page
  {\thispagestyle{empty}
  \raggedleft
  {\huge\bfseries\sffamily \textcolor[rgb]{0.7,0.7,0.7}{Paper {\fontsize{80}{100}\selectfont \Roman{paper}}}} \\
  \vspace{8mm}
  \centering
  {\huge\bfseries\sffamily \the\papertitle}  \\
  \vspace{4mm plus 0.1fill}
  {\large\sffamily{\the\paperauthor}} \\
  \vspace{8mm plus 0.2fill}
  \textbf{Abstract} \\
  \vspace{2mm}
  \parbox{0.9\columnwidth}{\par \normalsize \the\paperabstract}
  \vfill
  \raggedright
  \noindent
  {\footnotesize Published in: \\
  {\slshape \the\paperpublishedin},
  \if\relax\the\papereditor\relax \else
    {\the\papereditor}
  \fi
  \the\paperyear \\
  \the\paperaddendum}
  \newpage}
  
  % Overwrite label and ref
%   \let\labelold\label
%   \renewcommand{\label}[1]{\labelold{a##1}}
%   \let\refold\ref
%   \renewcommand{\ref}[1]{\refold{a##1}}
%   \let\crefold\cref
%   \renewcommand{\cref}[1]{\crefold{a##1}}

  % Adapt paths and include file
  \graphicspath{{\the\paperfolder}}
  \makeatletter
  \def\input@path{{\the\paperfolder}}
  \makeatother
  \if \the\paperfile\empty
  \else
    \ifcompilepapers
      \include{\the\paperfolder\the\paperfile}
    \fi
  \fi
}


