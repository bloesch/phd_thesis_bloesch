%====================================================================================================
%=========================================== Observability ==========================================
\section{Observability Analysis}
\label{sec:obsv}


%====================================================================================================
%============================================= Nonlinear ============================================
\subsection{Nonlinear Observability Analysis}
Analyzing the observability characteristics of the underlying nonlinear system reveals the theoretical limitations of state estimation and can validate the employed approach. Based on the paper of Hermann and Krener \cite{Hermann1977} a nonlinear observability analysis is performed. In order to remain analytically tractable robocentric coordinates are introduced. The coordinate transformation is bijective and will thus not change the observability characteristics. Given the current operating point by
\begin{align}
\label{eqn:stavecop}
	\b{x}^* := \begin{pmatrix}
		\b{r}^* &
		\b{v}^* &
		\b{q}^* &
		\b{p}_{1}^* &
		\cdots &
		\b{p}_{N}^* &
		\b{b}_f^* &
		\b{b}_\omega^*
	\end{pmatrix}
\end{align}
the following coordinate transformation is introduced:
\begin{align}
\label{eqn:cootra}
	\b{z} := \begin{bmatrix}
		\b{s}_{1} \\
		\vdots \\
		\b{s}_{N} \\
		\bar{\b{v}} \\
		\bar{\b{b}}_\omega \\
		\bar{\b{q}} \\
		\bar{\b{b}}_f \\
		\bar{\b{r}}
	\end{bmatrix} = \begin{bmatrix}
		\b{C}(\b{p}_{1} - \b{r}) \\
		\vdots \\
		\b{C}(\b{p}_{N} - \b{r}) \\
		\b{C} \b{v} \\
		\b{b}_\omega - \b{b}_\omega^* \\
		\b{q} \otimes \b{q}^{*-1} \\
		\b{b}_f - \b{b}_f^* \\
		\b{C} \b{r}
	\end{bmatrix}.
\end{align}
The quantities in (\ref{eqn:cootra}) are ordered such that a nice row echelon form results. The corresponding prediction model (\ref{eqn:conpre})-(\ref{eqn:conpre2}) and measurement equation (\ref{eqn:legkinmeas}) will be transformed to
\begin{align}
	\dot{\b{z}} := {} & \begin{bmatrix}
		(\b{\omega} - \bar{\b{b}}_\omega)^\times \b{s}_{1} - \bar{\b{v}} \\
		\vdots \\
		(\b{\omega} - \bar{\b{b}}_\omega)^\times \b{s}_{N} - \bar{\b{v}} \\
		(\b{\omega} - \bar{\b{b}}_\omega)^\times \bar{\b{v}} + \b{f} - \bar{\b{b}}_f + \bar{\b{C}} \b{C}^* \b{g} \\
		0 \\
		\b{\Omega}(\b{\omega} - \bar{\b{b}}_\omega) \bar{\b{q}} \\
		0 \\
		(\b{\omega} - \bar{\b{b}}_\omega)^\times \bar{\b{r}} + \bar{\b{v}} 
	\end{bmatrix}, \\
	\tilde{\b{s}}_{i} = {} & \b{s}_{i}  \qquad    i \in \{1,\ldots,N\}
\end{align}
where all noise terms were disregarded as they have no influence on the observability and where $\bar{\b{C}}$ and $\b{C}^*$ represent the rotation matrices corresponding to $\bar{\b{q}}$ and to $\b{q}^*$, respectively.

The observability of the transformed system can now be analyzed. In contrast to the linear case, Lie-derivatives need to be computed in order to evaluate the observability matrix. By applying a few row-operations  and by directly including the transformed operating point
\begin{align}
\label{eqn:cootraop}
	\b{z}^* := \begin{pmatrix}
		\b{s}_{1}^* &
		\cdots &
		\b{s}_{N}^* &
		\b{C}^* \b{v}^* &
		0 &
		(0 \ 0 \ 0 \ 1) &
		0 &
		\b{C}^* \b{r}^*
	\end{pmatrix}
\end{align}
the observability matrix can be converted into a row echelon form. For the sake of readability the $^*$ are dropped again:
\begin{align}
	\b{\mathcal{O}} = {} & \begin{bmatrix}
		\mathbb{I} & \cdots & 0 & 0 & 0 & 0 & 0 & 0 \\
		\vdots & \ddots & \vdots & \vdots & \vdots & \vdots & \vdots & \vdots \\
		0 & \cdots & \mathbb{I} & 0 & 0 & 0 & 0 & 0 \\
		0 & \cdots & 0 & -\mathbb{I} & 0 & 0 & \b{s}_{1}^\times & 0 \\
		0 & \cdots & 0 & 0 & \mathbb{I} & -2 (\b{C} \b{g})^\times & \b{\mathcal{O}}_1 & 0 \\
		0 & \cdots & 0 & 0 & 0 & 2 \b{\omega}^\times (\b{C} \b{g})^\times & \b{\mathcal{O}}_2 & 0 \\
		0 & \cdots & 0 & 0 & 0 & 0 & \b{\Delta s}_{i,j}^\times & 0 \\
		0 & \cdots & 0 & 0 & 0 & 0 & \b{\Delta s}_{i,j}^\times \b{\omega}^\times & 0 \\
		0 & \cdots & 0 & 0 & 0 & 0 & \b{\Delta s}_{i,j}^\times \b{\omega}^\times \b{\omega}^\times & 0 \\
		0 & \cdots & 0 & 0 & 0 & 0 & \b{\mathcal{O}}_3 & 0 \\
		0 & \cdots & 0 & 0 & 0 & 0 & \vdots & 0 
	\end{bmatrix},
\end{align}
\begin{align}
	\b{\mathcal{O}}_1 = {} & -\b{s}_{1}^\times \b{\omega}^\times - 2 (\b{C} \b{v})^\times, \\
	\b{\mathcal{O}}_2 = {} & (\b{s}_{1}^\times \b{\omega}^\times + 3 (\b{C} \b{v})^\times ) \b{\omega}^\times - \b{\omega}^\times (\b{s}_{1}^\times \b{\omega}^\times + 2 (\b{C} \b{v})^\times) \nonumber\\
	{} &  - (\b{C} \b{g})^\times - 2 \b{f}^\times, \\
	\b{\mathcal{O}}_3 = {} & \b{\omega}^\times (\b{s}_{1}^\times \b{\omega}^\times \b{\omega}^\times + 5 (\b{C} \b{v})^\times \b{\omega}^\times - 4 \b{f}^\times - 3 (\b{C} \b{g})^\times) \nonumber\\
	{} & - (\b{s}_{1}^\times \b{\omega}^\times \b{\omega}^\times + 4 (\b{C} \b{v})^\times \b{\omega}^\times - 3 \b{f}^\times - 2 (\b{C} \b{g})^\times) \b{\omega}^\times \nonumber\\
	{} & - 4 \b{\omega}^\times (\b{C} \b{v}) \b{\omega}^T,
\end{align}
\begin{align}
	\b{\Delta s}_{i,j} := {} & \b{s}_{i} - \b{s}_{j}.
\end{align}
A full interpretation of this matrix is not within the scope of this paper. However, two essential points are emphasized. The four dimensional manifold composed of robot position and yaw angle (rotation around gravity vector $\b{g}$) is \emph{always} unobservable. This can be verified by looking at the tangential space spanned by the matrix
\begin{align}
	\bar{\b{U}} &= \begin{bmatrix}
		0 & \cdots & 0 & 0 & 0 & 0 & 0 & \mathbb{I} \\
		0 & \cdots & 0 & 0 & 0 & \frac{1}{2}(\b{C} \b{g})^T & 0 & 0
	\end{bmatrix}^T, \\
	0 &= \b{\mathcal{O}} \bar{\b{U}}.
\end{align}
Infinitesimal errors $\b{\Delta z} = \bar{\b{U}} \b{\epsilon}$ lying within the subspace of $\bar{\b{U}}$ cannot be detected. Transforming this back to our original coordinates yields the tangential space
\begin{align}
\label{eqn:nonlinunobsvss}
	\b{U} &= \begin{bmatrix}
		\b{C}^T & 0 & 0 & \b{C}^T & \cdots & \b{C}^T & 0 & 0 \\
		\b{g}^T \b{r}^\times & \b{g}^T \b{v}^\times & \b{g}^T \b{C}^T & \b{g}^T \b{p}_{1}^\times & \cdots & \b{g}^T \b{p}_{N}^\times & 0 & 0
	\end{bmatrix}^T
\end{align}
where the upper row corresponds to a 3 dimensional translation of the inertial coordinate frame and where the lower row corresponds to a rotation of the inertial coordinate frame around the gravity axis $\b{g}$.

The second point is that in some cases, the rank loss associated with the unobservable manifold can increase by up to 5 additional ranks. Table \ref{tab:rankloss} depicts some of the cases. All cases which induce a rank loss require some singularities. It can thus be stated that statistically \emph{almost surely} the unobservable space will be limited to absolute position and yaw angle (except for the case where there is no ground contact at all). Note that if the bias estimation is excluded, the unobservable subspace will be \emph{invariantly of rank four}.

\begin{table}[t]
	\centering
	\begin{tabular}{|c|c|c|c|r|}
		$\b{\omega}$ & $\b{f}$ & $\b{v}$ & $\b{s}_{1}, \hdots, \b{s}_{N}$ & Rank loss \\
		\hline
		\rowcolor{lightgray} $\b{\omega} \cdot \b{C} \b{g} \neq 0$ & * & * & not co-linear & $0$ \\
		\hline
		$\b{\omega} \cdot \b{C} \b{g} \neq 0$ & \multicolumn{2}{|c|}{$\det{\b{\mathcal{O}}_3} \neq 0$} & at least one contact & $0$ \\
		\hline
		$\b{\omega} \cdot \b{C} \b{g} = 0$ & * & * & at least one contact & $\geq 1$ \\
		\hline
		0 & * & * & at least one contact & $\geq 2$ \\
		\hline
		\rowcolor{lightgray} 0 & * & * & not co-linear & $2$ \\
		\hline
		0 & 0 & * & $\b{s}_{1} = \hdots = \b{s}_{N}$ & $3$ \\
		\hline
		0 & $-1/2 \b{C} \b{g}$ & * & $\b{s}_{1} = \hdots = \b{s}_{N}$ & $5$ \\
		\hline
	\end{tabular}
	\caption{Estimation scenarios and corresponding rank loss.}
	\label{tab:rankloss}
\end{table}

Unfortunately, typical operating points can lie very close to singular cases. The upper highlighted row in table \ref{tab:rankloss} represents the case where the robot has at least 3 non co-linear ground contacts and where the rotation axis is not perpendicular to the gravity vector. The lower highlighted row represents the corresponding singular case where $\b{\omega} = 0$ inducing a rank loss of $2$. This proximity to singular cases can cause bad convergence quality. For this reason the filter is implemented in such a manner that the estimation of the accelerometer and gyroscope biases can be enabled or disabled at runtime. Thereby it is possible to disable the bias estimation for critical tasks. On the other hand special maneuvers can be derived from the conditions in table \ref{tab:rankloss} which can properly estimate the bias states.

%Unfortunately, typical operating points, are very close to singular cases (highlighted row in table \ref{tab:rankloss}) and consequently might exhibit bad stability qualities.

%====================================================================================================
%============================================ Constraints ===========================================
\subsection{Observability Analysis of the Extended Kalman Filter}
The filter makes use of a linearized and discretized version of the nonlinear system model:
\begin{align}
\label{eqn:linsys}
	\b{x}_{k+1} &= \b{F}_k \b{x}_{k} + \b{w}_{lin,k}, \\
\label{eqn:linsys2}
	\b{y}_{k} &= \b{H}_k \b{x}_{k} + \b{n}_{lin,k},
\end{align}
where errors caused by linearization or discretization are incorporated in the noise terms $\b{w}_{lin,k}$ and $\b{n}_{lin,k}$. The corresponding observability analysis will be performed by applying the concept of local observability matrices \cite{Chen1990}: similar to the time-invariant case the observable subspace can be derived by analyzing the subspace spanned by the rows of a local observability matrix:
\begin{align}
\label{eqn:locobsvmat}
	\b{\mathcal{M}} &= \begin{bmatrix}
		\b{H}_k \\
		\b{H}_{k+1} \b{F}_k \\
		\b{H}_{k+2} \b{F}_{k+1} \b{F}_k \\
		\b{H}_{k+3} \b{F}_{k+2} \b{F}_{k+1} \b{F}_k \\
		\vdots
	\end{bmatrix}.
\end{align}

The observability characteristics of the discrete linear time-varying system (\ref{eqn:linsys})-(\ref{eqn:linsys2}) can differ from those of the underlying nonlinear system (\ref{eqn:conpre})-(\ref{eqn:conpre2}),(\ref{eqn:legkinmeas}). This discrepancy can be caused by linearization/discretization effects as well as by noise effects. The effect of noise becomes particularly evident when contemplating the observability characteristics of a corresponding noiseless (ideal) system. For the presented system the effect of noise renders the yaw angle observable by preventing the evaluation of the Jacobians $\b{F}_k$ and $\b{H}_k$ around the true state and thereby increasing the numerical rank of the local observability matrix $\b{\mathcal{M}}$. The spurious appearance of new observable states is strongly objectionable as it results in overconfident state estimation. The magnitude of this inconsistency depends on the noise ratio, but in the long run, it will always deteriorate the state estimate.

The above phenomenon has been observed earlier in the context of EKF-SLAM \cite{Huang2008,Julier2001}. Huang et al. \cite{Huang2010} introduced the Observability Constrained Extended Kalman Filter in order to tackle this issue. The approach in this paper goes much along their idea: the unobservable subspace of the nonlinear system (\ref{eqn:nonlinunobsvss}) should also be unobservable in the linearized and discretized system (\ref{eqn:linsys})-(\ref{eqn:linsys2}). Mathematically, this can be imposed by adding the following constraint:
\begin{align}
\label{eqn:obsvcon}
	\b{\mathcal{M}} \ \b{U} &= 0.
\end{align}
In order to meet this constraint Huang et al. evaluate the Jacobians at special operating points: instead of using the actual state estimate they use slightly altered values.

The approach in this paper tackles the observability problem by exploiting the following observation: the noiseless case does meet the constraint (\ref{eqn:obsvcon}) because it perfectly fulfills the prediction equations (\ref{eqn:ekfstapre})-(\ref{eqn:ekfstapre2}) and thus the appropriate terms are canceled out. For the presented filter it suffices if the following constraints are introduced (where a ${}^*$ denotes the states or measurements around which Jacobians are evaluated):
\begin{align}
\label{eqn:precon}
	\b{r}_{k+1}^* &= \b{r}_{k}^* + \Dt \b{v}_{k}^* + \frac{\Dt^2}{2} (\b{C}_k^{*T}\b{f}_{k,1}^*+\b{g}), \\
	\b{v}_{k+1}^* &= \b{v}_{k}^* + \Dt (\b{C}_k^{*T}\b{f}_{k,2}^*+\b{g}), \\
\label{eqn:precon2}
	\b{q}_{k+1}^* &= \b{\zeta}(\b{\omega}_k^*) \otimes \b{q}_{k}^*, \\
	\b{p}_{i,k+1}^* &= \b{p}_{i,k}^* \ \ \ \ \forall i \in \{1,\ldots,N\}.
\end{align}
Both, filter state and IMU measurements, are allowed to differ from their actual estimated quantities. However, in order to keep the linearization errors small the linearization point should remain as close as possible to the estimated state. Thus, given the timestep $l_i$ of the last touch-down event of foot $i$, the first-ever available estimate is chosen for the linearization:
\begin{align}
	{} & \b{r}_{k}^* = \b{r}_{k}^-, \ \ \ \
	\b{v}_{k}^* = \b{v}_{k}^-, \ \ \ \
	\b{q}_{k}^* = \b{q}_{k}^-, \\
	{} & \b{p}_{i,k}^* = \b{p}_{i,l_i}^- \ \ \forall i \in \{1,\ldots,N\}.
\end{align}
This is in analogy to the First-Estimates Jacobian EKF of Huang et al. \cite{Huang2008}. But, in general, the prediction constraints (\ref{eqn:precon})-(\ref{eqn:precon2}) are still not met. For this reason the additional terms $\b{f}_{k,1}^*$, $\b{f}_{k,2}^*$ and $\b{\omega}_{k}^*$ were introduced. Now, by choosing
\begin{align}
	\b{f}_{k,1}^* &= \b{C}_k^{*T}\left(\frac{\b{r}_{k+1}^* - \b{r}_{k}^* - \Dt \b{v}_{k}^*}{0.5 \Dt^2}  - \b{g} \right), \\
	\b{f}_{k,2}^* &= \b{C}_k^{*T}\left(\frac{\b{v}_{k+1}^* - \b{v}_{k}^*}{\Dt}  - \b{g} \right), \\
	\b{\omega}_k^* &= \b{\zeta}^{-1}\left(\b{q}_{k+1}^* \otimes \b{q}_{k}^{*-1}\right)
\end{align}
all constraints can be easily met. The above quantities represent the IMU measurements that would arise when considering two subsequent filter prediction states at timestep $k$ and $k+1$. Because the acceleration related measurements can differ if evaluated based on the position prediction or on the velocity prediction, two terms were introduced. This permits to keep the computation of the linearization quantities simple and avoids complex optimization algorithms or oscillation provoking bindings between subsequent linearization points.

Computing the Jacobians $\b{F}_k$ and $\b{H}_k$ using the supplementary linearization quantities and evaluating the corresponding local observability matrix (\ref{eqn:locobsvmat}) yields:
\setlength\arraycolsep{1.4pt}
\begin{align*}
	\b{\mathcal{M}} &= \begin{bmatrix}
		-\mathbb{I} & 0 & \b{s}_{1,k}^\times \b{C}_k^T & \mathbb{I} & \cdots & 0 & 0 & 0 \\
		\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots \\
		-\mathbb{I} & 0 & \b{s}_{1,k}^\times \b{C}_k^T & 0 & \cdots & \mathbb{I} & 0 & 0 \\
		0 & \mathbb{I} & (\b{v}_k + \frac{\Dt}{2} \b{g})^\times \b{C}_k^T & 0 & 0 & 0 & -\frac{\Dt^2}{2} \b{C}_k^T & \# \\
		0 & 0 & -\b{g}^\times & 0 & 0 & 0 & \frac{1}{2} (\b{C}_{k+1}^T+\b{C}_k^T) & \# \\
		0 & 0 & 0 & 0 & 0 & 0 & \frac{1}{2} (\b{C}_{k+2}^T-\b{C}_k^T) & \# \\
		0 & 0 & 0 & 0 & 0 & 0 & \frac{1}{2} (\b{C}_{k+3}^T-\b{C}_{k+1}^T) & \# \\
		0 & 0 & 0 & 0 & 0 & 0 & \vdots & \#
	\end{bmatrix}
\end{align*}
whereby it is simple to test that the observability constraint (\ref{eqn:obsvcon}) is satisfied. As a last side note: similarly to the nonlinear case, observability rank loss will again be induced when $\b{\omega} \equiv 0$ and thus
\setlength\arraycolsep{2.5pt}
\begin{align}
	\b{C}_{k+2}^T-\b{C}_k^T = 0.
\end{align}

% Link to EKF properties
