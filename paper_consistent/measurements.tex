%====================================================================================================
%============================================== Sensors =============================================
\section{Sensor Devices and Measurement Models}
\label{sec:meas}
%The main purpose of the state estimation is to enable precise stabilization and navigation of the high compliant series elastic actuated (SEA \cite{Pratt1995}) quadruped robot StarlETH \cite{Hutter2011}. Without any feedback action, the torque controlled mechanical systems is unstable in the upright standing equilibrium. Consequently, small perturbations need to be detected and proactively corrected.

This section discusses the required sensors and the corresponding stochastic measurement models for a $N$ legged robot. The particular model choices represent a trade-off between simplicity and accuracy. Throughout the paper, external disturbances and noise will be modeled as continuous white Gaussian noise or as discrete Gaussian noise processes. This is a coarse simplification, but can be handled by increasing the corresponding covariance matrix.

%====================================================================================================
%============================================= Encoders =============================================
\subsection{Forward Kinematics and Encoders}
Incremental encoders provide access to the angular position of all joints. The corresponding encoder measurement vector $\tilde{\b{\alpha}}$ of the joint angles vector $\b{\alpha}$ is assumed to be affected by discrete Gaussian noise $\b{n}_{\alpha}$ with covariance matrix $\b{R}_\alpha$:
\begin{eqnarray}
\label{eqn:encmeasmod}
	\tilde{\b{\alpha}} = \b{\alpha} + \b{n}_{\alpha}.
\end{eqnarray}
%The position of the $i$th contact point w.r.t. the main body frame $\b{B}$ is modeled by
Based on the known leg kinematics, the location of each foot can be computed with respect to the main body. However, due to erroneous calibration and possible errors in the kinematical model $\b{\lkin}_i(\cdot)$ of leg $i$, additive discrete Gaussian noise terms $\b{n}_{s,i}$ are included in the model:
\begin{eqnarray}
\label{eqn:kinmeasmod}
	\b{s}_{i} = \b{\lkin}_i(\b{\alpha}) + \b{n}_{s,i},
\end{eqnarray}
where $\b{s}_{i}$ represents the vector from the center of the main body to the contact point of leg $i$ and where $\b{R}_s$ is the covariance matrix of $\b{n}_{s,i}$. Both, $\b{s}_{i}$ and $\b{n}_{s,i}$, are expressed in the body fixed frame $\b{B}$.

%Given the footpoint locations there would be simple methods which would directly return the pose of the main robot, e.g., simple least squares fitting assuming the feet to lie on a horizontal plane. Most of these methods have the following shortcomings: the estimated pose is only with respect to the current footpoint position (no absolute roll or pitch estimate possible) and they can not handle the case were the number of contact points do not fully define the pose of the robot.

%====================================================================================================
%================================================ IMU ===============================================
\subsection{Inertial Sensors}
The IMU measures the proper acceleration $\b{f}$ and the angular rate $\b{\omega}$ of the robot's main body. The proper acceleration is related to the absolute acceleration $\b{a}$ by
\begin{align}
	\label{eqn:proacc}
	\b{f} = \b{C}(\b{a}-\b{g}),
\end{align}
where $\b{C}$ is the matrix rotating coordinates of a vector expressed in the inertial coordinate frame $\b{I}$ into the body coordinate frame $\b{B}$.
The IMU quantities $\b{f}$ and $\b{\omega}$ are assumed to be directly measured in the body coordinate frame $\b{B}$.
%The IMU coordinate frame is assumed to be aligned with the body coordinate frame in which $\b{f}$ and $\b{\omega}$ are expressed as well.
In order to describe the underlying stochastic process, the following continuous stochastic models are introduced:
\begin{eqnarray}
	\tilde{\b{f}} &=& \b{f} + \b{b}_f + \b{w}_f, \\
	\dot{\b{b}}_f &=& \b{w}_{bf}, \\
	\tilde{\b{\omega}} &=& \b{\omega} + \b{b}_\omega + \b{w}_\omega, \\
	\label{eqn:gyrbias}
	\dot{\b{b}}_\omega &=& \b{w}_{b\omega}.
\end{eqnarray}
The measured quantities $\tilde{\b{f}}$ and $\tilde{\b{\omega}}$ are affected by additive white Gaussian noise processes $\b{w}_f$ and $\b{w}_\omega$ and by bias terms $\b{b}_f$ and $\b{b}_\omega$. The bias terms are modeled as Brownian motions and their derivatives can be represented by white Gaussian noise processes $\b{w}_{bf}$ and $\b{w}_{b\omega}$. The noise terms are specified by the corresponding covariance parameters $\b{Q}_f$, $\b{Q}_{bf}$, $\b{Q}_\omega$, and $\b{Q}_{b\omega}$. Following the paper of El-Sheimy et al. \cite{Sheimy2008}, they can be evaluated by examining the measured Allan variances. For the sake of simplicity each covariance parameter is assumed to be a diagonal matrix with identical diagonal entries.




