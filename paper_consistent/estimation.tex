%====================================================================================================
%============================================ Estimation ============================================
\section{State Estimation}
\label{sec:est}
As stated in the previous section, two different sources of data are available. Each of them provides information that can potentially contribute to the state estimate of the robot. In order to exploit this information an Extended Kalman Filter is designed. This section starts by defining the state vector of the filter and subsequently continues by formulating the filter models and equations.

%====================================================================================================
%=============================================== State ==============================================
\subsection{Filter State Definition}
The state vector of the filter has to be chosen such that the corresponding prediction and measurement equations can be stated in a clean and consistent manner. In this approach the state vector of the quadruped robot is composed of the position of the center of the main body $\b{r}$, of the corresponding velocity $\b{v}$ and of the quaternion $\b{q}$ representing the rotation from the inertial coordinate frame $\b{I}$ to the body coordinate frame $\b{B}$. In order to consider the kinematics of the legs, the absolute  positions of the $N$ foot contact points $\b{p}_{i}$ are included into the state vector. Together with the accelerometer bias $\b{b}_f$ and the gyroscope bias $\b{b}_\omega$ this yields the following state vector:
\begin{align}
\label{eqn:stavec}
	\b{x} := \begin{pmatrix}
		\b{r} &
		\b{v} &
		\b{q} &
		\b{p}_{1} &
		\cdots &
		\b{p}_{N} &
		\b{b}_f &
		\b{b}_\omega
	\end{pmatrix}.
\end{align}
$\b{r}$,$\b{v}$ and all contact positions $\b{p}_{i}$ are expressed in the inertial coordinate frame $\b{I}$, whereas $\b{b}_f$ and $\b{b}_\omega$ are expressed in the body coordinate frame $\b{B}$. Given a quaternion $\b{q}$ the corresponding rotation matrix $\b{C}$ can be easily determined.

The presented Extended Kalman Filter represents the uncertainties of the estimated state vector via the covariance matrix $\b{P}$ of the corresponding state error vector $\b{\delta x}$
\begin{align}
	\label{eqn:covmat}
	\b{P} := {} & \mathrm{Cov} (\b{\delta x}), \\
	\label{eqn:errsta}
	\b{\delta x} := {} & \begin{pmatrix}
		\b{\delta r} &
		\b{\delta v} &
		\b{\delta \phi} &
		\b{\delta p}_{1} &
		\cdots &
		\b{\delta p}_{N} &
		\b{\delta b}_f &
		\b{\delta b}_\omega
	\end{pmatrix}.
\end{align}
For the orientation state $\b{q}$, special care has to be taken. It possesses 3 degrees of freedom and its covariance term should therefore also be represented by a 3 dimensional covariance matrix. Therefore the error of the pose is represented as a 3-dimensional rotation vector $\b{\delta \phi}$.  That is, if $\hat{\b{q}}$ represents the estimate of the orientation quaternion, the error quaternion $\b{\delta q}$ is defined by the relation
\begin{align}
\label{eqn:errquat}
	\b{q} = \b{\delta q} \otimes \hat{\b{q}},
\end{align}
where $\otimes$ is the quaternion multiplication operator and where the quaternion error is related to the error rotation vector by means of the map $\b \zeta (\cdot)$:
\begin{align}
\label{eqn:errrotvec}
	\b{\delta q} &= \b \zeta(\b{\delta \phi}), \\
\label{eqn:rotvectoquat}
	\b \zeta: \b{v} \mapsto \b \zeta(\b v) &= \left[ \begin{array}{c}
		\sin(\frac{1}{2}\|\b{v}\|)\frac{\b{v}}{\|\b{v}\|} \\
		\cos(\frac{1}{2}\|\b{v}\|)
	\end{array} \right].
\end{align}

The inclusion of the foot contact positions into the filter state is the key point in the filter design, enabling a simple and consistent representation of the model equations. The leg kinematics measurements represent relative pose measurements between main body and foot contact, based on which the EKF is able to simultaneously correct the location of the foot contacts as well as the pose of the main body. In fact, the presented approach can be interpreted as a simultaneous localization and mapping (SLAM) algorithm, where the position of the actual foot contacts build up the map the robot is localized in.

%====================================================================================================
%============================================ Prediction ============================================
\subsection{Prediction model}
The prediction equations are responsible for propagating the state from one time\-step to the next. The IMU measurements $\tilde{\b{f}}$ and $\tilde{\b{\omega}}$ are directly included here. Using (\ref{eqn:proacc})-(\ref{eqn:gyrbias}), a set of continuous time differential equations can be formulated:
\begin{align}
\label{eqn:conpre}
	\dot{\b{r}} &= \b{v}, \\
	\dot{\b{v}} &= \b{a} = \b{C}^T (\tilde{\b{f}} - \b{b}_f - \b{w}_f) + \b{g}, \\
	\dot{\b{q}} &= \frac{1}{2} \b{\Omega}(\b{\omega}) \b{q} = \frac{1}{2} \b{\Omega}(\tilde{\b{\omega}} - \b{b}_\omega - \b{w}_\omega) \b{q}, \\
\label{eqn:conprefoot}
	\dot{\b{p}}_{i} &= \b{C}^T \b{w}_{p,i} \ \ \ \ \forall i \in \{1,\ldots,N\}, \\
	\dot{\b{b}}_f &= \b{w}_{bf}, \\
\label{eqn:conpre2}
	\dot{\b{b}}_\omega &= \b{w}_{b\omega},
\end{align}
where $\b{\Omega}(\cdot)$ maps an arbitrary rotational rate $\b{\omega}$ to the 4x4 matrix used for representing the corresponding quaternion rate:
\begin{align}
\label{eqn:omegatomat}
	\b \Omega: \b{\omega} \mapsto \b \Omega(\b \omega) = \begin{bmatrix}
		0 & \omega_z & -\omega_y & \omega_x \\
		-\omega_z & 0 & \omega_x & \omega_y \\
		\omega_y & -\omega_x & 0 & \omega_z \\
		-\omega_x & -\omega_y & -\omega_z & 0
	\end{bmatrix}.
\end{align}

While in principle the foot contacts are assumed to remain stationary, the white noise terms $\b{w}_{p,i}$ in (\ref{eqn:conprefoot}) with covariance parameter $\b{Q}_{p,i}$ are added to the absolute foot positions in order to handle a certain amount of foot slippage. It is described in the body frame which allows tuning the magnitude of the noise terms in the different directions relative to the quadruped orientation (\ref{eqn:conpronoifootmat}). Furthermore, the noise parameter of a certain foothold is set to \emph{infinity} (or to a very large value) whenever it has no ground contact. This enables the corresponding foothold to relocate and reset its position estimate when it regains ground contact, whereby the old foothold position is dropped from the estimation process. This is all that is required in order to handle intermittent contacts when stepping.
%whereby the information of an old foothold is dropped.
\begin{align}
	\label{eqn:conpronoifootmat}
	\b{Q}_{p,i} &= \begin{bmatrix}
		w_{p,i,x} & 0 & 0 \\
		0 & w_{p,i,y} & 0 \\
		0 & 0 & w_{p,i,z}
	\end{bmatrix}.
\end{align}

%====================================================================================================
%============================================ Measurement ===========================================
\subsection{Measurement Model}
Based on the kinematic model (\ref{eqn:kinmeasmod}) a transformed measurement quantity is introduced for each leg $i$:
\begin{align}
\label{eqn:kinmeasmod3}
	\tilde{\b{s}}_{i} &:= \b{\lkin}_{i}(\tilde{\b{\alpha}}) \\
	&\approx \b{\lkin}_{i}(\b{\alpha}) + \b{J}_{\lkin,i} \b{n}_{\alpha} \\
	&\approx \b{s}_{i} \underbrace{- \b{n}_{s,i} + \b{J}_{\lkin,i} \b{n}_{\alpha}}_{\b{n}_{i}}.
\end{align}
The linearized noise effect from the encoders (\ref{eqn:encmeasmod}) and the noise from the foothold position are joined into a new measurement noise quantity $\b{n}_{i}$ with covariance matrix $\b{R}_i$:
\begin{align}
	\label{eqn:measnoifootmat}
	\b{R}_i &= \b{R}_s + \b{J}_{\lkin,i} \b{R}_\alpha \b{J}_{\lkin,i}^T ,
\end{align}
where $\b{J}_{\lkin,i}$ is the Jacobian of the kinematics of leg $i$ with respect to the joint angles $\b{\alpha}_{i}$ of the same leg:
\begin{align}
\label{eqn:legkinjac}
	\b{J}_{\lkin,i} := \frac{\partial \b{\lkin}_i(\b{\alpha})}{\partial \b{\alpha}_{i}}  \qquad    i \in \{1,\ldots,N\}.
\end{align}

$\tilde{\b{s}}_{i}$ is the measurement of the position of the foot contact $i$ with respect to the body coordinate frame $\b{B}$ which can also be expressed  as the absolute position of the foot contact minus the absolute position of the robot rotated into the body frame.
\begin{align}
\label{eqn:legkinmeas}
	\tilde{\b{s}}_{i} = \b{C}(\b{p}_{i}-\b{r}) + \b{n}_{i}.
\end{align}

%====================================================================================================
%============================================== Filter ==============================================
\subsection{Extended Kalman Filter Equations}
For the subsequent linearization and discretization of the above models, the following auxiliary quantity is introduced:
\begin{align}
\label{eqn:sqewgamma}
\b{\Gamma}_n &:= \sum_{i=0}^\infty \frac{\Dt^{i+n}}{(i+n)!}\b{\omega}^{\times i},
\end{align}
where the $(\cdot)^\times$ superscript is used to represent the skew-symmetric matrix obtained from a vector. It draws on the series expansion of the matrix exponential. For $n=0$ it yields:
\begin{align}
\label{eqn:sqew0}
	\b{\Gamma}_0 = \sum_{i=0}^\infty \frac{\left({\Dt \b{\omega}}^{\times}\right)^{i}}{i!} = \exp{\left(\Dt \b{\omega}^{\times}\right)}.
\end{align}
This means that $\b{\Gamma}_0$ represents the incremental rotation matrix if rotating an arbitrary coordinate frame with a rotational rate of $-\b{\omega}$ for $\Dt$ seconds. There exists a closed form expression for $\b{\Gamma}_n$ that can be efficiently numerically evaluated (similar to Rodrigues' rotation formula). %Furthermore it can be proven for $n \geq 1$:
%\begin{align}
%	\label{eqn:sqewint}
%	\b{\Gamma}_{n}(\Dt) &= \int_0^{\Dt} \b{\Gamma}_{n-1}(\tau) d \tau \\
%	\label{eqn:sqewrank}
%	rank \left(\b{\Gamma}_{n}\right) &= 3 \ \ \Leftrightarrow \ \ \Dt \ \neq \ 0
%\end{align}

%====================================================================================================
%============================================ Prediction ============================================
\subsubsection{Prediction Step}
A standard filtering convention is employed: at time step $k$ the \emph{a priori} state estimate is represented by $\hat{\b{x}}_k^-$, the \emph{a posteriori} state estimate by $\hat{\b{x}}_k^+$. Assuming zero-order hold for the measured quantities $\tilde{\b{f}}_{k}$ and $\tilde{\b{\omega}}_{k}$, and neglecting the effect of the incremental rotation, equations (\ref{eqn:conpre})-(\ref{eqn:conpre2}) can be discretized to:
\begin{align}
\label{eqn:ekfstapre}
	\hat{\b{r}}_{k+1}^- &= \hat{\b{r}}_{k}^+ + \Dt \hat{\b{v}}_{k}^+ + \frac{\Dt^2}{2} (\hat{\b{C}}_k^{+T}\hat{\b{f}}_k+\b{g}), \\
	\hat{\b{v}}_{k+1}^- &= \hat{\b{v}}_{k}^+ + \Dt (\hat{\b{C}}_k^{+T}\hat{\b{f}}_k+\b{g}), \\
	\hat{\b{q}}_{k+1}^- &= \b{\zeta}(\Dt \hat{\b{\omega}}_k) \otimes \hat{\b{q}}_{k}^+, \\
	\hat{\b{p}}_{i,k+1}^- &= \hat{\b{p}}_{i,k}^+ \ \ \ \ \forall i \in \{1,\ldots,N\}, \\
	\hat{\b{b}}_{f,k+1}^- &= \hat{\b{b}}_{f,k}^+, \\
	\hat{\b{b}}_{\omega,k+1}^- &= \hat{\b{b}}_{\omega,k}^+,
\label{eqn:ekfstapre2}
\end{align}
with the bias corrected IMU measurements
\begin{align}
	\hat{\b{f}}_k &= \tilde{\b{f}}_{k}-\hat{\b{b}}_{f,k}^+, \\
	\hat{\b{\omega}}_k &= \tilde{\b{\omega}}_{k}-\hat{\b{b}}_{\omega,k}^+.
\end{align}

In order to correctly propagate the covariance matrix through the state dynamics, a set of linear differential equations describing the error dynamics is derived from (\ref{eqn:conpre})-(\ref{eqn:conpre2}) where all higher order terms were neglected:
\begin{align}
	\label{eqn:conerrpro}
	\dot{\b{\delta r}} &= \b{\delta v}, \\
	\dot{\b{\delta v}} &= -\b{C}^T \b{f}^\times \b{\delta \phi} - \b{C}^T \b{\delta b}_f - \b{C}^T \b{w}_{f}, \\
	\dot{\b{\delta \phi}} &= -\b{\omega}^\times \b{\delta \phi} - \b{\delta b}_\omega - \b{w}_{\omega}, \\
	\dot{\b{\delta p}}_{i} &= \b{C}^T \b{w}_{p,i} \ \ \ \ \forall i \in \{1,\ldots,N\}, \\
	\label{eqn:conerrpro2}
	\dot{\b{\delta b}}_f &= \b{w}_{bf}, \\
	\dot{\b{\delta b}}_\omega &= \b{w}_{b\omega}.
\end{align}
For the subsequent discretization, Van Loan's results \cite{Loan1978} and the relation (\ref{eqn:sqewgamma}) can be applied to get the discrete linearized error dynamics matrix $\b{F}_k$ and the discrete process noise covariance matrix $\b{Q}_k$ (for readability only one foothold estimate is depicted):
\begin{align}
\b{F}_k &= \begin{bmatrix}
		 \mathbb{I} & \Dt\mathbb{I} & -\frac{\Dt^2}{2} \hat{\b{C}}_k^{+T} \hat{\b{f}}_k^\times & 0 & -\frac{\Dt^2}{2} \hat{\b{C}}_k^{+T} & 0 \\
		 0 & \mathbb{I} & -\Dt \hat{\b{C}}_k^{+T} \hat{\b{f}}_k^\times & 0 & -\Dt \hat{\b{C}}_k^{+T} & 0 \\
		 0 & 0 & \hat{\b{\Gamma}}_{0,k}^T & 0 & 0 & -\hat{\b{\Gamma}}_{1,k}^T \\
		 0 & 0 & 0 & \mathbb{I} & 0 & 0 \\
		 0 & 0 & 0 & 0 & \mathbb{I} & 0 \\
		 0 & 0 & 0 & 0 & 0 & \mathbb{I}
	\end{bmatrix},
\end{align}
\begin{align*}
{} & \left[ \begin{array}{ccc}
		 \frac{\Dt^3}{3} \b{Q}_{f} + \frac{\Dt^5}{20} \b{Q}_{bf} & \frac{\Dt^2}{2} \b{Q}_{f} + \frac{\Dt^4}{8} \b{Q}_{bf} & 0 \\
		 \frac{\Dt^2}{2} \b{Q}_{f} + \frac{\Dt^4}{8} \b{Q}_{bf} & \Dt \b{Q}_{f} + \frac{\Dt^3}{3} \b{Q}_{bf} & 0 \\
		 0 & 0 & \Dt \b{Q}_{\omega} + (\hat{\b{\Gamma}}_{3,k} + \hat{\b{\Gamma}}_{3,k}^T) \b{Q}_{b\omega} \\
		 0 & 0 & 0 \\
		 -\frac{\Dt^3}{6} \b{Q}_{bf} \hat{\b{C}}_k^+ & -\frac{\Dt^2}{2} \b{Q}_{bf} \hat{\b{C}}_k^+  & 0 \\
		 0 & 0 & -\b{Q}_{b\omega} \hat{\b{\Gamma}}_{2,k}
	\end{array} \right. \nonumber\\
{} & \left. \begin{array}{ccc}
		 0 & -\frac{\Dt^3}{6} \hat{\b{C}}_k^{+T} \b{Q}_{bf} & 0 \\
		 0 & -\frac{\Dt^2}{2} \hat{\b{C}}_k^{+T} \b{Q}_{bf} & 0 \\
		 0 & 0 & -\hat{\b{\Gamma}}_{2,k}^T \b{Q}_{b\omega} \\
		 \Dt \hat{\b{C}}_k^{+T} \b{Q}_{p} \hat{\b{C}}_k^+ & 0 & 0 \\
		 0 & \Dt \b{Q}_{bf} & 0 \\
		 0 & 0 & \Dt \b{Q}_{b\omega}
	\end{array} \right] = \b{Q}_k.
\end{align*}
By linearly combining two Gaussian distributions the Extended Kalman Filter stipulates the following \emph{a priori} estimate of the covariance matrix at the timestep $k+1$:
\begin{align}
\label{eqn:ekfcovpre}
	\b{P}_{k+1}^- = \b{F}_k \b{P}_k^+ \b{F}_k^T + \b{Q}_k.
\end{align}

%====================================================================================================
%============================================== Update ==============================================
\subsubsection{Update Step}
The measurement residual, also called innovation, is the difference between actual measurements and their predicted value:
\begin{align}
\label{eqn:ekfinn}
	\b{y}_k := \begin{pmatrix}
		\tilde{\b{s}}_{1,k} - \hat{\b{C}}_k^-(\hat{\b{p}}_{1,k}^--\hat{\b{r}}_k^-) \\
		\vdots \\
		\tilde{\b{s}}_{N,k} - \hat{\b{C}}_k^-(\hat{\b{p}}_{N,k}^--\hat{\b{r}}_k^-)
	\end{pmatrix}.
\end{align}
Considering the error states and again neglecting all higher order terms, it can be derived that the errors of the predicted leg kinematics measurements are given by:
\begin{align}
\label{eqn:legkinpreest2}
	\b{s}_{i,k} - \hat{\b{C}}_k^-(\hat{\b{p}}_{i,k}^--\hat{\b{r}}_k^-) \approx & {} - \hat{\b{C}}_k^- \b{\delta r}_k^- + \hat{\b{C}}_k^- \b{\delta p}_{i,k}^- \nonumber\\
{} &  + \left(\hat{\b{C}}_k^-(\b{p}_{i,k}^- -\b{r}_k^-)\right)^\times \b{\delta \phi}_k^-.
\end{align}
With this the measurement Jacobian $\b{H}_k$ can be evaluated:
\begin{align}
	\b{H}_k &= \frac{\partial\b{y}_k}{\partial\hat{\b{x}}_k} \nonumber\\
	&= \begin{bmatrix}
		- \hat{\b{C}}_k^- & 0 & \left(\hat{\b{C}}_k^-(\hat{\b{p}}_{1,k}^--\hat{\b{r}}_k^-)\right)^\times & \hat{\b{C}}_k^- & \cdots & 0 & 0 & 0 \\
		\vdots & \vdots & \vdots & \vdots & \ddots & \vdots & \vdots & \vdots \\
		- \hat{\b{C}}_k^- & 0 & \left(\hat{\b{C}}_k^-(\hat{\b{p}}_{N,k}^--\hat{\b{r}}_k^-)\right)^\times & 0 & \cdots & \hat{\b{C}}_k^- & 0 & 0
	\end{bmatrix}. \nonumber
\end{align}
Stacking the single measurement noise matrices (\ref{eqn:measnoifootmat}) returns the total measurement noise matrix:
\begin{align}
\label{eqn:measnoista}
	\b{R}_k &= \begin{bmatrix}
		\b{R}_{1,k} & & \\
		& \ddots & \\
		& & \b{R}_{N,k}
	\end{bmatrix}.
\end{align}

Finally the \emph{a priori} state estimate can be merged with the current measurements, where the Extended Kalman Filter states the following update equations:
\begin{align}
\label{eqn:ekfup}
	\b{S}_k &:= \b{H}_k \b{P}_k^- \b{H}_k^T + \b{R}_k, \\
	\b{K}_k &:= \b{P}_k^- \b{H}_k^T \b{S}_k^{-1}, \\
	\b{\Delta x}_k &:= \b{K}_k \b{y}_k, \\
	\label{eqn:ekfcovup}
	\b{P}_k^+ &:= (\b{I} - \b{K}_k \b{H}_k) \b{P}_k^-
\end{align}
where $\b{S}_k$ represents the innovation covariance, $\b{K}_k$ the Kalman gain, $\b{\Delta x}_k$ the resulting correction vector and $\b{P}_k^+$ the \emph{a posteriori} estimate of the state covariance matrix. Given $\b{\Delta x}_k$ the state estimate can be updated. Again the orientation state requires special attention. Although the quaternion is of dimension 4, the extracted rotational correction $\b{\Delta \phi}_k$ has only 3 dimensions. It basically represents the 3D rotation vector that needs to be applied to correct the predicted quaternion:
\begin{align}
\label{eqn:ekfrotcor}
	\hat{\b{q}}_k^+ = \b \zeta(\b{\Delta \phi}_k) \otimes \hat{\b{q}}_k^-.
\end{align}


